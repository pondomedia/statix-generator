const fs = require('fs-extra')
let babelOptions = fs.readJSONSync('./.babelrc')
babelOptions.presets[0][1] = { targets : { node: true}}
require('@babel/register')(babelOptions)

module.exports = require('./_webpack.config.babel').default