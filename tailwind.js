const merge = require('deepmerge')
let theme = {}

try {
    theme = require('./generated/theme.json')
}
catch(e){}

module.exports = {
    important: true,
    theme: {
        fontFamily: {
            sans: ['Roboto']
        },
        screens: {
            sm: "640px",
            md: "1072px",
            xl: "1452px",
            xxl: "1832px"
        },
        extend: merge({
            colors: {
                "white-overlay": 'rgba(255,255,255, 0.05)'
            },
            spacing: {
                96: '24rem',
                14: '3.5rem'
            },
            borderRadius: {
                xlg: '1rem'
            }
        }, theme)
    },
    variants: {},
    plugins: []
}