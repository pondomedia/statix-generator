import resolveConfig from 'tailwindcss/resolveConfig'
import tailwindConfig from '../tailwind'

export default resolveConfig(tailwindConfig)