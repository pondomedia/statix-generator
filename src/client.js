window.isServer = false

import LazyLoad from "vanilla-lazyload"
new LazyLoad({
    elements_selector: ".lazy",
    threshold: 800,
    load_delay: 200
});
import '../generated/font/iconfont.css'
import './index.css';

import React from 'react'
import AppContainer from "./App";
import ReactDom from 'react-dom'

let initialData = window.INITIAL_DATA || {}
ReactDom.hydrate(<AppContainer {...initialData} />, document.getElementById('root'))