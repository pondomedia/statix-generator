import React, { createContext, useMemo, useContext, useState, useEffect, useCallback } from 'react'
import {Helmet} from "react-helmet";
import useCollapse from 'react-collapsed'
import tailwind from './resolvedTailwind'
import classes from './App.module.css'
import useWindowSize from './useWindowSize'
import clsx from 'clsx'
import ReactSVG from 'react-svg'

// SVGs
import logotipoPondo from './svg/logotipo-pondo.svg'
import isotipoPondo from './svg/isotipo-pondo.svg'
import externalLinkIcon from './svg/external-link-square-alt-solid.svg'
import barsIcon from './svg/bars-solid.svg'
import angleDownIcon from './svg/angle-down-solid.svg'
import angleUpIcon from './svg/angle-up-solid.svg'
import timesIcon from './svg/times-solid.svg'

import { useSwipeable } from 'react-swipeable'

export const InitialDataCtx = createContext()
export const AppCtx = createContext()

function CircularButton({children, className, onClick }){
  return <div className={clsx("select-none hover:bg-white h-16 w-16 p-4 rounded-full flex items-center justify-center cursor-pointer", className)} onClick={onClick}>
    {children}
  </div>
}

function useObject(object){
  return useMemo(()=>object, Object.values(object))
}

function TopBar(){
  let { site } = useContext(InitialDataCtx)
  let { leftSideBarOpen, toggleLeftSideBar, rightSideBarOpen, toggleRightSideBar} = useContext(AppCtx)
  let { width } = useWindowSize()

  let hidden = width < parseInt(tailwind.theme.screens.xl) && (leftSideBarOpen || rightSideBarOpen)

  let classNames = useMemo(()=>{
    let ret = ""
    if(width > parseInt(tailwind.theme.screens.xl) && 
    width < parseInt(tailwind.theme.screens.xxl)){
      if(leftSideBarOpen){
        ret = "md:pl-96"
      }
      else if(rightSideBarOpen){
        ret = "md:pr-96"
      }
    }
    return ret
  }, [width, leftSideBarOpen, rightSideBarOpen])

  return <div className="pl-5 pr-5 pt-5">
    <div className={clsx(
      "relative h-16 flex flex-row flex-no-wrap items-center",
      !hidden && leftSideBarOpen && 'pl-96',
      !hidden && rightSideBarOpen && 'pr-96',
      !hidden && 'w-full',
      hidden && 'invisible w-0 p-0 overflow-hidden'
    )} style={{transition: 'padding 500ms' }}>
      <CircularButton className="mr-auto" onClick={toggleLeftSideBar}>
        <ReactSVG src={barsIcon} className="text-primary w-4"/>
      </CircularButton>
      <a href="/" title={site.name} className={clsx("w-full absolute top-0 left-0 h-16 flex items-center justify-center pointer-events-none", classNames)} style={{transition: "padding-left, padding-right", transitionDuration: "500ms"}}>
        <img src={site.logoPositivo} className="max-h-full pointer-events-auto cursor-pointer" />
      </a>
      <CircularButton className="ml-auto" onClick={toggleRightSideBar}>
        <ReactSVG src={isotipoPondo} className="text-primary" beforeInjection={svg=>{
          svg.setAttribute('style', null)
          svg.setAttribute('fill', 'currentColor')
        }}/>
      </CircularButton>
    </div>
  </div>
}

function LeftSideBarSection({ section, isOpen, setOpenedSection, id}){
  const {getCollapseProps, getToggleProps} = useCollapse({ isOpen })
  let rcToggleId = `react-collapsed-toggle-${id}`
  let rcPanelId = `rc-panel-${id}`
  
  return <div className="mb-3" >
    <div className="left-sidebar-section" {...getToggleProps({
      onClick: ()=>{ isOpen ? setOpenedSection(null) : setOpenedSection(id)}
    })} id={rcToggleId} aria-controls={rcPanelId}>
      <div className="mr-4 w-4">
        <i className={`w-full icon icon-${section.iconName} flex items-center`} />
      </div>
      <span className="text-lg">
        {section.title}
      </span>
      <div className="ml-auto">
        <ReactSVG src={isOpen ? angleUpIcon : angleDownIcon} className="text-white w-3"/>
      </div>
    </div>
    <div className="pl-12" {...getCollapseProps()} id={rcPanelId}>
      <div>
        { section.items.map((link, i) => {

          return <a className="left-sidebar-item" href={link.url} title={link.name} key={i}>{link.name}</a>
        } )}
      </div>
    </div>
  </div>
}

function RightSideBar(){
  let { rightSideBarOpen, toggleRightSideBar} = useContext(AppCtx)
  let swipeHandlers = useSwipeable({ onSwipedRight: toggleRightSideBar })
  let { width } = useWindowSize()
  let { sites } = useContext(InitialDataCtx)

  return <>
    
    <div className={clsx("fixed top-0 right-0 w-full overflow-y-auto z-40 max-w-sm h-screen", classes.RightSideBar, !rightSideBarOpen && classes.RightSideBar__Closed)} style={{backgroundColor: '#393939'}} {...swipeHandlers}>
      <div className="flex flex-col p-5 justify-center min-h-screen">
        <div className="w-full flex justify-center mb-8 relative">
          <div className="absolute h-full left-0 flex items-center text-white opacity-50 cursor-pointer" onClick={toggleRightSideBar}>
            <ReactSVG src={timesIcon} className="text-white absolute w-4 h-4"/>
          </div>
          <div className="h-16 w-48 flex items-center">
            <ReactSVG src={logotipoPondo} beforeInjection={svg=>{
              svg.setAttribute('style', null)
              svg.setAttribute('fill', '#FFDF40')
            }}/>
          </div>
        </div>
        <div className="sites">
          { sites.map( (s,i) => <a href={'https://'+s.domain} target="_blank" rel="noreferrer noopener" key={i} className={classes.SiteLink}>
              <div className="mr-4 w-4">
                <img className="w-full" src={s.favicon}/>
              </div>
              <span className="text-lg">{s.name}</span>
              <ReactSVG src={externalLinkIcon} className="ml-auto w-3" />
              {/* <div className="ml-auto"><FontAwesomeIcon width="0" icon={faExternalLinkSquareAlt} /></div> */}
            </a>
          )}
        </div>
        <div className="footer mt-auto text-center font-normal text-xs flex flex-col items-center pt-6" style={{color:'#525252'}}>
          <ReactSVG src={isotipoPondo} className="text-primary w-14 mb-6" beforeInjection={svg=>{
              svg.setAttribute('style', null)
              svg.setAttribute('fill', '#525252')
          }}/>
          <div className="font-bold">Contacto</div>
          <div className="mb-3">admin@pondomedia.com</div>
          <div>Cada contenido es propiedad de su respectivo autor. Pondomedia no se atribuye el derecho de todo el
            contenido mostrado en esta página web. Reclamos, consultas y sugerencias al contacto.
          </div>
        </div>
      </div>
    </div>
    { width < parseInt(tailwind.theme.screens.xl) && rightSideBarOpen ? 
      <div className="fixed bg-black opacity-50 top-0 left-0 right-0 bottom-0 z-20 cursor-pointer" onClick={toggleRightSideBar}>
      </div> : null
    }
  </>
}

function LeftSideBar(){
  let { leftSideBarOpen, toggleLeftSideBar } = useContext(AppCtx)
  let { site } = useContext(InitialDataCtx)
  let swipeHandlers = useSwipeable({ onSwipedLeft: toggleLeftSideBar })
  let [openedSection, setOpenedSection] = useState(null)
  let { width } = useWindowSize()

  useEffect(()=>{
    let currSection = site.sections.findIndex(s => s.items.some(i=>i.url === document.location.href))
    setOpenedSection(currSection)
  }, [])

  return <>
    <div className={clsx("fixed top-0 left-0 w-full p-5 h-screen overflow-y-auto bg-primary z-40 max-w-sm flex flex-col", classes.LeftSideBar, !leftSideBarOpen && classes.LeftSideBar__Closed)} {...swipeHandlers}>
      <div className="w-full flex justify-center mb-5 relative">
        <div className="absolute h-full right-0 pr-6 flex items-center text-white opacity-50 cursor-pointer" onClick={toggleLeftSideBar}>
          <ReactSVG src={timesIcon} className="text-white absolute w-4 h-4"/>
        </div>
        <div className="h-16 w-32">
          <a href="/" title={site.name}>
            <img src={site.logoNegativo}/>
          </a>
        </div>
      </div>
      <div className="sections">
        { site.sections.map( (s,i) => <LeftSideBarSection section={s} isOpen={i===openedSection} setOpenedSection={setOpenedSection} id={i} key={i}/>)}
      </div>
      <If condition={site.injectLeftSideBarBottom}>
        <div className="mt-auto" dangerouslySetInnerHTML={{__html: window.isServer ? site.injectLeftSideBarBottom : '' }} />
      </If>
    </div>
    { width < parseInt(tailwind.theme.screens.xl) && leftSideBarOpen ? 
      <div className="fixed bg-black opacity-50 top-0 left-0 right-0 bottom-0 z-20 cursor-pointer" onClick={toggleLeftSideBar}>
      </div> : null
    }
  </>
}

function Content(){
  let { site, page } = useContext(InitialDataCtx)
  const {getCollapseProps, getToggleProps, isOpen} = useCollapse()
  let hasToc = page.toc && page.toc.length
  
  return <div className="max-w-5xl bg-white w-full pt-2 sm:pt-10 rounded-xlg flex flex-col items-center justify-center pb-10 mb-10 px-2 md:px-0">
    <article className="max-w-3xl w-full content">
      <div className="rounded-xlg mb-10 bg-primary">
        <h1 className={clsx("relative font-sans text-xl font-bold text-white leading-normal block text-center p-5 pr-10", hasToc && "cursor-pointer")}  {...hasToc && getToggleProps()}>
          {page.title}
          <If condition={hasToc}>
            <div className="absolute right-0 top-0 flex items-center h-full text-white pr-5">
              <ReactSVG src={isOpen ? angleUpIcon : angleDownIcon} className="text-white w-3"/>
            </div>
          </If>
        </h1>
        <If condition={hasToc}>
          <div className="toc px-5 pb-2" {...getCollapseProps()}>
            {
              page.toc.map( item => <a href={'#'+item.id} className={classes.TocLink}>
                {item.text}
              </a>)
            }
          </div>
        </If>
      </div>
      <div className="px-2 md:px-0 post-content" dangerouslySetInnerHTML={{__html: window.isServer ? page.content.replace() : '' }}>
      </div>
    </article>
  </div>
}


function App(){
  let { site, page } = useContext(InitialDataCtx)
  let { leftSideBarOpen, rightSideBarOpen } = useContext(AppCtx)
  let { width } = useWindowSize()

  useEffect(()=>{
    let withCover = width < parseInt(tailwind.theme.screens.xl) && (leftSideBarOpen || rightSideBarOpen)
    window.document.body.classList.toggle('with-cover', withCover)
  }, [width, rightSideBarOpen, leftSideBarOpen])

  let classNames = useMemo(()=>{
    let ret = ""
    if(width > parseInt(tailwind.theme.screens.xl) && 
    width < parseInt(tailwind.theme.screens.xxl)){
      if(leftSideBarOpen){
        ret = "md:pl-96"
      }
      else if(rightSideBarOpen){
        ret = "md:pr-96"
      }
    }
    return ret
  }, [width, leftSideBarOpen, rightSideBarOpen])

  return <div className="bg-gray-200 min-h-screen">
    <Helmet>
      <title>{page.metaTitle}</title>
      <If condition={page.metaDescription}>
        <meta name="description" content={page.metaDescription}/>
      </If>
    </Helmet>
    <LeftSideBar />
    <RightSideBar />
    <TopBar />
    <div className={clsx("w-full flex justify-center pt-5 px-1 md:px-0 sm:px-5", classNames)} style={{transition: "padding-left, padding-right", transitionDuration: "500ms"}}>
      <If condition={site.injectOverPageTitle}>
        <div className="mb-10" dangerouslySetInnerHTML={{__html: window.isServer ? site.injectOverPageTitle : '' }} />
      </If>
      <Content />
    </div>
  </div>
}

function AppContainer(initialData){
  let [ leftSideBarOpen, setLeftSideBarOpen ] = useState(false)
  let [ rightSideBarOpen, setRightSideBarOpen ] = useState(false)
  let { width } = useWindowSize()
  
  let inBounds = useMemo(()=>{
    return width < parseInt(tailwind.theme.screens.xxl) &&
      leftSideBarOpen &&
      rightSideBarOpen
  }, [width, leftSideBarOpen, rightSideBarOpen])
  
  useEffect(()=>{
    if(inBounds){ setRightSideBarOpen(()=>false) }
  }, [leftSideBarOpen, width])

  useEffect(()=>{
    if(inBounds){ setLeftSideBarOpen(()=>false) }
  }, [rightSideBarOpen])

  let toggleLeftSideBar = useCallback(()=>{
    setLeftSideBarOpen(v => !v)
  }, [])

  let toggleRightSideBar = useCallback(()=>{
    setRightSideBarOpen(v=>!v)
  }, [])

  let appCtx = useObject({
    leftSideBarOpen,
    setLeftSideBarOpen,
    rightSideBarOpen,
    setRightSideBarOpen,
    toggleLeftSideBar,
    toggleRightSideBar
  })

  return <InitialDataCtx.Provider value={initialData}>
    <AppCtx.Provider value={appCtx}>
      <App />
    </AppCtx.Provider>
  </InitialDataCtx.Provider>
}

export default AppContainer