window.isServer = true

import React from 'react'
import ReactDomServer from 'react-dom/server'
import {Helmet} from "react-helmet";
import AppContainer from './App'
import parse from 'html-react-parser';
import urlJoin from 'url-join'

export default (data) => {
  const { publicPath } = data.webpackStats.compilation.outputOptions
  const initialData = {
    path: data.path,
    site: data.site,
    page: data.pages[data.path],
    sites: data.sites
  }
  
  const { site, page } = initialData

  const initialDataClient = {
    ...initialData,
    page: {
      ...initialData.page,
      content: undefined
    }
  }

  let home = 'https://' + site.domain
  let canonical = urlJoin(home, page.slug)

  function HTML () {
    const htmlAttrs = helmet.htmlAttributes.toComponent();
    const bodyAttrs = helmet.bodyAttributes.toComponent();

    let author = {
      "@type": "Organization",
      name: "Pondomedia",
      logo: {
        "@type": "ImageObject",
        url: "https://statix-pondomedia.pondomedia.com/logotipo+pondo.png"
      }
    }

    let schema = {
      "@context":"http://schema.org",
      "@type":"BlogPosting",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": canonical
      },
      headline: page.title,
      author,
      publisher: author,
      description: page.metaDescription
    }

    if(page.images){
      schema.image = page.images.map(image => image.src)
    }

    if(page.modifiedOn){
      schema.dateModified = page.modifiedOn
    }

    if(page.createdOn){
      schema.datePublished = page.createdOn
    }
    

    return (
        <html {...htmlAttrs}>
          <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link href={canonical} rel="canonical" />
            <link href={home} rel="home" />
            {svg.map(f => <link href={publicPath + f} rel="preload" as="fetch" type="image/svg+xml" crossorigin="crossorigin"></link>)}
            {css.filter(f => !f.startsWith('server')).map(file => <link type="text/css" href={publicPath + file} rel="preload" as="style"></link>)}
            <meta name="theme-color" content={data.site.primaryColor} />
            <meta name="mobile-web-app-capable" content="yes" />
            {helmet.title.toComponent()}
            {helmet.meta.toComponent()}
            {helmet.link.toComponent()}
            {js.filter(f => !f.startsWith('server')).map(file => <script src={publicPath + file} defer></script>)}
            {css.filter(f => !f.startsWith('server')).map(file => <link type="text/css" href={publicPath + file} rel="stylesheet"></link>)}
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet"/>
            <link rel="shortcut icon" href={data.site.favicon} />
            <script dangerouslySetInnerHTML={{__html:`//<!DATA[\nwindow.INITIAL_DATA=${JSON.stringify(initialDataClient).replace(/</g,'&lte;').replace(/>/g, '&gte;')}`}} />
            <script type="application/ld+json" dangerouslySetInnerHTML={{__html: JSON.stringify(schema, null, 2)}} />
            <If condition={data.site.injectHead}>
              {parse(data.site.injectHead)}
            </If>
          </head>
          <body {...bodyAttrs}>
            <If condition={data.site.injectBodyStart}>
              {parse(data.site.injectBodyStart)}
            </If>
            <div id="root" dangerouslySetInnerHTML={{__html: reactResult}}>
            </div>
            <If condition={data.site.injectBodyEnd}>
              {parse(data.site.injectBodyEnd)}
            </If>
          </body>
        </html>
    );
  }


  const assets = Object.keys(data.webpackStats.compilation.assets);
  const css = assets.filter(value => value.match(/\.css$/));
  const js = assets.filter(value => value.match(/\.js$/));
  const svg = assets.filter(value => value.match(/\.svg$/));

  Helmet.canUseDOM = false
  let reactResult = ReactDomServer.renderToString(<AppContainer {...initialData} />)
  let helmet = Helmet.renderStatic()

  return '<!DOCTYPE html>'+ReactDomServer.renderToString(<HTML />)
}
