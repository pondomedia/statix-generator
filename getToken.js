import fs from 'fs-extra'
import path from 'path'
import fetch from 'node-fetch'
import urlJoin from 'url-join'
import dotenv from 'dotenv'
dotenv.config()

let tokenCache
let apiUrl = process.env.API_URL
let cacheDir = './.cache'
let tokenPath = path.join(cacheDir, 'AUTH_TOKEN')
fs.ensureDirSync(cacheDir)

async function getNewToken(){
    let request = await fetch(urlJoin(apiUrl, 'auth'), {
        method: 'POST',
        body: JSON.stringify({ username: process.env.API_USERNAME, password: process.env.API_PASSWORD }),
        headers: { 'content-type': 'application/json' }
    })
    
    let json = await request.json()
    return json.authToken
}

async function isTokenAuthorized(token){
    let request = await fetch(urlJoin(apiUrl, 'auth'), {
        headers: { 'authorization' : `bearer ${token}` }
    })
    
    let json = await request.json()
    return json.authorized
}

async function saveAndGetNewToken(){
    let newToken = await getNewToken()
    await fs.writeFile(tokenPath, newToken, 'utf-8')
    tokenCache = newToken
    return newToken
}

async function getToken(){
    if(tokenCache){
        return await isTokenAuthorized(tokenCache) ? tokenCache : saveAndGetNewToken()
    }

    if(!await fs.exists(tokenPath)){
        return saveAndGetNewToken()
    }
    
    let token = await fs.readFile(tokenPath, 'utf-8')
    if(await isTokenAuthorized(token)){
        return token
    }

    return saveAndGetNewToken()
}

export default getToken