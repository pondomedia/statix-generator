const tailwindcss = require('tailwindcss')
const cssnano = require('cssnano')
const purgecss = require('@fullhuman/postcss-purgecss')({

    // Specify the paths to all of the template files in your project 
    content: [
      './src/**/*.html',
      './src/**/*.js',
      './*.js'
    ],
    css: [
      './src/index.css',
      'generated/font/iconfont.css'
    ],
    whitelistPatterns: [ /icon-.+/ ],
    // Include any special characters you're using in this regular expression
    defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
  })

module.exports = {
    plugins: [
        require('postcss-easy-import'),
        tailwindcss('./tailwind.js'),
        require('autoprefixer'),
        ...process.env.NODE_ENV === 'production'
            ? [purgecss, cssnano({ preset: 'default' })]
            : [],
        require('postcss-calc')

    ]
}