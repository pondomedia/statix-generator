import dotenv from 'dotenv'
import fetch from 'node-fetch'
import path from 'path'
import urlJoin from 'url-join'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import MiniCssExtractPlugin from "mini-css-extract-plugin"
import TerserPlugin from 'terser-webpack-plugin'
import StaticSiteGeneratorPlugin from 'static-site-generator-webpack-plugin'
import jsdom from 'jsdom'
import wget from 'node-wget'
import fs from 'fs-extra'
import webfontsGenerator from 'vusion-webfonts-generator'
import btoa from 'btoa'
import color from 'color'
import cheerio from 'cheerio'
import slug from 'slug'
import _ from 'lodash'
import getToken from './getToken'
import { createSitemapsAndIndex } from 'sitemap'

slug.defaults.mode = 'rfc3986'

dotenv.config()
const { JSDOM } = jsdom;
const dom = new JSDOM();
const NODE_ENV = process.env.NODE_ENV || 'development'
const isProd = NODE_ENV === 'production'
const isDev = !isProd
const isDevServer = process.env.WEBPACK_DEV_SERVER

fs.ensureDirSync('./public/i/')
let BASE_URL = process.env.BASE_URL || '/'

async function getPages({authToken, apiUrl, site}){
  let injectInline = site.injectInline

  let pages = await fetch(apiUrl + `pages?siteId=${site._id}&includeHome=true&status[]=published`, {
      headers: { Authorization: authToken }
    }).then(r => r.json())
  
  await Promise.all(pages.map(async (page,i) => {
    if(page.isHome){
      page.slug=''
    }
    
    let $ = cheerio.load(page.content)
    let toc = []
    // pass through every image
    //  - download the image
    //  - rename the image with a sha1 from the origin url
    //  - move to public folder
    //  - relink every image
    //$('h1,h2,h3,h4,h5,h6,h7,h8')
    $('h2')
      .each((i, el)=>{
          el = $(el)
          let text = el.text()
          let id = slug(text)
          el.attr('id', id)
          toc.push({ text, id })
      })
    
    $('p').each((i, el)=>{
      let html = $(el).html()
      
      if(html === '&#xA0;' || html === '&nbsp;'){
        $(el).removeClass()
        $(el).addClass('nbsp')
        $(el).html('')
      }
    })

    let maxAds = 4
    if(injectInline){
      $('body').children('p,blockquote,figure').each((i, el)=>{
        if(i%8===0 && maxAds-- > 0){
          $(el).before(injectInline)
        }
      })
    }

    page.images = []
    
    await Promise.all($('img').toArray().map(async (el)=>{
      el = $(el)
      let src = el.attr('src').replace('statix-pondomedia.s3.us-east-2.amazonaws.com', 'statix-pondomedia.pondomedia.com')
      let alt = el.attr('alt')

      page.images.push({src, alt})
      el.attr('data-src', src)
      el.attr('width', 600)
      el.attr('height', 600)
      el.addClass('lazy')
      el.addClass('loading-anim')
      el.attr('src', "")
      el.attr('src', "/srcFix.png")
    }))
    

    page.toc = toc
    page.content = $('body').html()
    return
  }))
  
  let pagesById = {}
  let pagesMap = {}

  for(let page of pages){
    pagesById[page._id] = page
    pagesMap['/' + page.slug] = page
  }

  return {
    pagesMap,
    pagesById
  }
}

export default async function(env){
  let apiUrl = process.env.API_URL
  let baseUrl = BASE_URL
  let token = await getToken()
  
  const authorization = `bearer ${token}`

  
  let sites = (await fetch(urlJoin(apiUrl, 'sites'), {
    headers: { authorization }
  }).then( r => r.json()))
  
  let site = sites.find(site => site._id === process.env.SITE_ID)

  let { pagesById, pagesMap } = await getPages({ authToken: authorization, apiUrl, site })
  
  // Generate sitemap
  createSitemapsAndIndex({
    urls: Object.entries(pagesMap).map(([pathName, page]) => {
      let toRet = {
        url: urlJoin(BASE_URL, pathName),
      }
      if(page.modifiedOn){
        toRet.lastmod = page.modifiedOn
      }

      if(page.images && page.images.length){
        toRet.img = page.images.map(image => {
          let retImage = { url: image.src }
          if(image.alt){
            retImage.title = image.alt
          }

          return retImage
        })
      }

      return toRet
    }),
    targetFolder: './public',
    hostname: BASE_URL.replace(/\/$/, ""),
    sitemapName: 'sitemap',
    sitemapSize: 1000,
    gzip: false
  })

  site.sections = (site.sections || []).map(section => {
    section.items = (section.items || []).map(item => {
      let url = ''
      if(item.linkType === 'pageLink' && item.page && pagesById[item.page._id]){
        let linkedPage = pagesById[item.page._id]
        url = urlJoin(baseUrl, linkedPage.slug || '')
      }
      else if(item.linkType === 'customLink ' && item.link ){
        url = item.link
      }
      
      return { ...item, url }
    })
    return section
  })

  await fs.ensureDir('./generated/')
  await fs.ensureDir('./generated/include/')
  await fs.ensureDir('./.cache/')
  let primaryColor = site.primaryColor || '#000'
  let lighterPrimaryColors = {}
  for(let i of _.range(1,10)){
    lighterPrimaryColors[i] = color(primaryColor).lightness(i*10).hex()
  }

  await fs.writeJSON('./generated/theme.json', {
    colors: {
      primary: site.primaryColor || '#000',
      'primary-l': lighterPrimaryColors,
      'primary-light': color(primaryColor).lightness(98).hex()
    }
  })
  await fs.ensureDir('./.cache/icons/')
  let sections = site.sections.filter(s=>s.icon)


  let icons = await Promise.all(sections.map(async s =>{
    let icon = path.basename(s.icon)
    let filePath = path.join('./.cache/icons/', path.basename(icon))
    if(await fs.exists(filePath)){
      return filePath
    }
  
    return new Promise((ok, fail)=>{
      console.log(`Downloading icon: ${icon}`)
      wget({ url: s.icon, dest: './.cache/icons/' }, (err,data)=>{
        if(err){ fail(err); return; }
        ok(data.filepath)
      })
    })
  }));

  Promise.all((site.injectFiles || []).map(async f => {
    return new Promise(async (ok, fail)=>{
      console.log(`Downloading file: ${f.file}`)
      let dir = path.join('./generated/include/', f.location, '/')
      await fs.ensureDir(dir)
      let dest = f.name ? path.join(dir, f.name) : dir

      wget({url:f.file, dest}, (err,data)=> err ? fail(err) : ok(data.filePath))
    })
  }))

  // icons.push(...(await fs.readdir('./include/svg/')).map(f => path.join('./include/svg/', f)))

  site.sections = sections.map( (s,i) => ({
    ...s,
    iconName: path.basename(icons[i], '.svg')
  }))

  await fs.ensureDir('./generated/font')
  await new Promise((ok, fail)=> {
    webfontsGenerator({
      files: icons,
      dest: './generated/font',
      normalize: true,
      fixedWidth: true,
      centerHorizontally: true
    }, (err) => err ? fail(err) : ok() )
  })

  /**
   * DO BEFORE
   * [x] get site info
   * [x] download all svgs
   * [x] compile svgs to webfont
   * [x] build the menu with the generated font codes
   * [x] include webfont
   */
  let config = {
    mode: NODE_ENV,
    node: { fs: 'empty' },
    devtool: isDev && 'eval-source-map',
    context: __dirname,
    entry  : {
      server: './src/server.js',
      client: './src/client.js'
    },
    output : {
      path: path.join(__dirname, 'dist'),
      filename: '[name].[hash].js',
      chunkFilename: '[id]_chunk.js',
      publicPath: '/',
      libraryTarget: 'umd'
    },
    resolve: {
      unsafeCache: true,
      extensions: [ ".js", ".json", ".jsx", ".html", ".css", ".scss" ],
      modules: [ 'node_modules' ]
    },
    module : {
      rules: [
        {
          test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
                publicPath: urlJoin(baseUrl, 'assets'),
                outputPath: 'assets'
              },
            }
          ],
          exclude: [ /\.(js|mjs|jsx|ts|tsx|html|json)$/ ],
      },
        { test: /\.(js|jsx)$/, exclude: /node_modules/, loader: 'babel-loader' },
        {
          test: /\.css$/i,
          use: [
            isProd ? MiniCssExtractPlugin.loader : 'style-loader',
            { 
              loader: 'css-loader',
              options: { sourceMap: isDev, importLoaders: 1 }
            },
            'postcss-loader'
          ],
          exclude: [ /\.module\.css$/ ],
        },
        {
          test: /\.module.css$/i, 
          use: [
              isProd ? MiniCssExtractPlugin.loader : 'style-loader',
              { loader: 'css-loader',
                options: {
                  sourceMap: isDev,
                  importLoaders: 1,
                  modules: {
                    mode: 'local',
                    localIdentName: '[local]--[hash:base64:5]',
                  },
                }
              },
              'postcss-loader'
          ]
        },
        { test: /\.(png|jpg|jpeg|gif)$/, loader: 'url-loader?limit=8192' },
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new CopyWebpackPlugin([
        {from: 'public/', to: '.' },
        {from: 'generated/include/', to: '.'}
      ]),
      new StaticSiteGeneratorPlugin({
        entry: 'server',
        paths: Object.keys(pagesMap),
        locals: {
          pages: pagesMap,
          site,
          sites: sites.map(site => ({
            name: site.name,
            domain: site.domain,
            favicon: site['favicon-small']
          }))
        },
        globals: {
          window: dom.window,
          document: dom.window.document,
          navigator: dom.window.navigator,
          btoa
        },
      }),
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].css',
      }),
    ],
    optimization: {
      splitChunks: {
        chunks(chunk){
          return chunk.name !== 'server'
        },
        maxInitialRequests: Infinity,
        minSize: 20000,
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              // get the name. E.g. node_modules/packageName/not/this/part.js
              // or node_modules/packageName
              const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
  
              // npm package names are URL-safe, but some servers don't like @ symbols
              return `npm.${packageName.replace('@', '')}`;
            },
          }
        }
      },
      runtimeChunk: false,
      minimizer: [new TerserPlugin({
        terserOptions: { 
          output: { comments: false }
        }
      })],
    },
    ...isDevServer && {
      devServer: {
        host: '0.0.0.0',
        historyApiFallback: true,
        inline: false,
        https: { ... await require('https-localhost')().getCerts() }
      }
    } 
  };

  return config
}